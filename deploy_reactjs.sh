#!/bin/bash
#----------------shell script for deploy reactjs project--------------------
fun_deployreact(){
mkdir $1
chmod 777 $1
cd $1
#-----clone project-----
git clone $2 && cd $(basename $_ .git)
#-----build project-----
npm install
npm run build
printf 'FROM nginx:alpine\nCOPY ./nginx.conf /etc/nginx/conf.d/default.conf\nRUN rm -rf /usr/share/nginx/html/*\nCOPY ./build /usr/share/nginx/html\nEXPOSE 3000\nENTRYPOINT ["nginx","-g","daemon off;"]'>Dockerfile
printf 'server {\nlisten 80;\nlocation /{\nroot /usr/share/nginx/html;\nindex index.html index.html;\ntry_files $uri $uri/ /index.html;\n}\nerror_page 500 502 503 504 /50x.html;\nlocation = /50x.html{\n root /usr/share/nginx/html;\n}\n}'>nginx.conf
#-----build and push images----
docker build -t 19112000/$1 .
docker login -u 19112000 -p Rathkhmer2021
docker push 19112000/$1
#-----remote and schedule kubernetes work-----
sshpass -p 123 ssh -o StrictHostKeyChecking=no devops@34.124.223.237 'bash shell.sh '$1
}
#-----calling function-----
fun_deployreact $1 $2