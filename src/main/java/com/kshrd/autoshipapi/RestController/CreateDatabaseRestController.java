package com.kshrd.autoshipapi.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
public class CreateDatabaseRestController {

    private final ProcessBuilder builder = new ProcessBuilder();
    private final String token1 = "1";


    //========================================================================================//
    //                                         MySql                                          //
    //========================================================================================//
    //  Requirement for MySql database:                                                       //
    //         database Name                                                                  //
    //         Username                                                                       //
    //         password                                                                       //
    //         number on connection                                                           //
    //****************************************************************************************//

    @PostMapping("/api/v3/mySql/{token}/{containerName}/{databaseName}/{userName}/{password}/{connectionNum}/{port}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> mySql(@PathVariable("token") String token,
                                   @PathVariable("containerName") String containerName,
                                   @PathVariable("databaseName") String databaseName,
                                   @PathVariable("userName") String userName,
                                   @PathVariable("password") String password,
                                   @PathVariable("connectionNum") int connectionNum,
                                   @PathVariable("port") int port) {
        Map<String, Object> response = new HashMap<>();
        builder.command("sh", "-c", "/bin/bash ./workspace/deploy_mysql.sh " + containerName + " " + databaseName + " " + userName + " " + password + " "+ connectionNum +" "+port);
        return getResponseEntity(token, databaseName, userName, password, connectionNum, response);
    }



    //========================================================================================//
    //                                      Postgres                                          //
    //========================================================================================//
    //  Requirement for Postgres database:                                                    //
    //         database Name                                                                  //
    //         Username                                                                       //
    //         password                                                                       //
    //         number on connection                                                           //
    //****************************************************************************************//

    @PostMapping("/api/v3/postgers/{token}/{containerName}/{databaseName}/{userName}/{password}/{connectionNum}/{port}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> postgresql(@PathVariable("token") String token,
                                        @PathVariable("containerName") String containerName,
                                        @PathVariable("databaseName") String databaseName,
                                        @PathVariable("userName") String userName,
                                        @PathVariable("password") String password,
                                        @PathVariable("connectionNum") int connectionNum,
                                        @PathVariable("port") int port){
        Map<String, Object> response = new HashMap<>();
        builder.command("sh", "-c", "/bin/bash ./workspace/deploy_postgres.sh " + containerName + " " + databaseName + " " + userName + " " + password + " "+ connectionNum +" "+port);
        return getResponseEntity(token, databaseName, userName, password, connectionNum, response);
    }


    //========================================================================================//
    //                                      ResponseEntity                                    //
    //========================================================================================//
    //  Template for responsive:                                                              //
    //         database Name                                                                  //
    //         Username                                                                       //
    //         password                                                                       //
    //         number on connection                                                           //
    //****************************************************************************************//

    private ResponseEntity<?> getResponseEntity(@PathVariable("token") String token,
                                                @PathVariable("databaseName") String databaseName,
                                                @PathVariable("userName") String userName,
                                                @PathVariable("password") String password,
                                                @PathVariable("connectionNum") int connectionNum,
                                                Map<String, Object> response) {
        if (Objects.equals(token, token1)) {
            if (userName != null) {
                if(databaseName != null) {
                    if(password != null) {
                        if(connectionNum > 0) {
                            try {
                                builder.start();
                            } catch (IOException e) {
                                System.out.println("error: " + e);
                            }
                            response.put("status", "successful");
                            response.put("message", "you have created database");
                            response.put("database name", databaseName);
                            response.put("username",userName);
                            response.put("password", password);
                            response.put("connection number", connectionNum);
                            response.put("success", true);
                        }else{
                            response.put("status", "ERROR");
                            response.put("message", "connection must be greater than 0");
                            response.put("success", false);
                        }
                    }else{
                        response.put("status", "ERROR");
                        response.put("message", "password cannot empty");
                        response.put("success", false);
                    }
                }else{
                    response.put("status", "ERROR");
                    response.put("message", "database name cannot empty");
                    response.put("success", false);
                }
            }else{
                response.put("status", "ERROR");
                response.put("message", "username cannot empty");
                response.put("success", false);
            }
        } else {
            response.put("status", "ERROR");
            response.put("message", "permission denied");
            response.put("success", false);
        }
        return ResponseEntity.ok().body(response);
    }


    //========================================================================================//
    //                                      Delete database                                   //
    //========================================================================================//
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @DeleteMapping("/api/v3/deleteDatabase/{token}/{databaseName}")
    public ResponseEntity<?> deleteDatabase(@PathVariable("databaseName") String databaseName,@PathVariable("token") String token){

        Map<String, Object> response = new HashMap<>();

        if(Objects.equals(token,token1)) {
            try {
                builder.command("sh", "/bin/bash ./shellscript/deleteDB.sh " + databaseName);
                builder.start();
            } catch (IOException e) {
                System.out.println("Error: " + e);
            }
            response.put("status","success");
            response.put("message", "you have deleted database");
            response.put("database name", databaseName);
        }else{
            response.put("status", "ERROR");
            response.put("message", "permission denied");
            response.put("success", false);
        }
        return ResponseEntity.ok().body(response);
    }
    @PostMapping("/api/v3/stopDatabase/{token}/{databaseName}")
    public ResponseEntity<?> stopDatabase(@PathVariable("databaseName")String databaseName,
                                          @PathVariable("token")String token){

        Map<String, Object> response = new HashMap<>();

        if(Objects.equals(token, token1)){
            if(databaseName != null){
                try{
                    builder.command("sh","/bin/bash/ ./shellscript/stopDb.sh " +databaseName);
                    builder.start();
                }catch (IOException e){
                    response.put("status","exception");
                    response.put("message","invalid");
                    response.put("success",false);
                }
                response.put("status","success");
                response.put("message","you have successfully delete database");
                response.put("database name", databaseName);
                response.put("success",true);
            }else{
                response.put("status", "fail");
                response.put("message", "you need to fill in data name");
                response.put("success", false);
            }
        }else{
            response.put("status", "ERROR");
            response.put("message", "permission denied");
            response.put("success", false);
        }
        return ResponseEntity.ok().body(response);
    }


}
