package com.kshrd.autoshipapi.RestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
public class DeployerRestController {
    private final ProcessBuilder builder = new ProcessBuilder();
    private final String token1 = "1";


    //========================================================================================//
    //                                      ResponseEntity                                    //
    //========================================================================================//
    //  Template for responsive deployment:                                                   //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    private ResponseEntity<?> responseEntity(@PathVariable("projectname")String projectName,
                                             @PathVariable("token")String token,
                                             Map<String, Object> response){
        if (Objects.equals(token, token1)) {
            if(projectName != null){
                try{
                    builder.start();
                }catch (IOException e){
                    response.put("status", "ERROR");
                    response.put("message", "error exception");
                    response.put("application name", projectName);
                    response.put("exception",e);
                    response.put("success",false);
                }
                response.put("status","success");
                response.put("message","congratulation! you have successfully deployed");
                response.put("application name", projectName);
                response.put("success",true);

            }else{
                response.put("status","ERROR");
                response.put("message","you need to fill application name");
                response.put("success",false);
            }
        }else{
            response.put("status","ERROR");
            response.put("message","permission denied");
            response.put("application name", projectName);
            response.put("success",false);
        }
        return ResponseEntity.ok().body(response);
    }


    //========================================================================================//
    //                                      war                                             //
    //========================================================================================//
    //  Deployment in maven project:                                                          //
    //        type: gradle or maven                                                           //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/api/v3/war/{token}/{projectName}/{linkGit}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> jarFile(@PathVariable("projectName") String projectName,
                                   @PathVariable("token")String token,
                                     @RequestParam("linkGit")String linkGit){

        Map<String, Object> response = new HashMap<>();
        builder.command("sh", "-c", "/bin/bash ./workspace/deploy_war.sh " + projectName+' '+linkGit).inheritIO();
//        builder.command("bash", "-c", "sshpass -p 123 ssh -o StrictHostKeyChecking=no devops@34.126.133.229 'bash deploy_angular.sh' "+projectName+' '+linkGit).inheritIO();
        return responseEntity(projectName,token, response);
    }
    //========================================================================================//
    //                                      jar                                            //
    //========================================================================================//
    //  Deployment in Gradle project:                                                         //
    //          type: gradle or maven                                                         //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/api/v3/jar/{token}/{projectName}/{linkGit}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> warFile(@PathVariable("projectName") String projectName,
                                    @PathVariable("token")String token,
                                     @RequestParam("linkGit")String linkGit){
        Map<String, Object> response = new HashMap<>();
//        builder.command("sh", "-c", "/bin/bash ./shellscript/warFile.sh " + type + " " +projectName);
        builder.command("sh", "-c", "/bin/bash ./workspace/deploy_jar.sh " + projectName+' '+linkGit).inheritIO();
        return responseEntity(projectName,token,response);
    }
    //========================================================================================//
    //                                      Reactjs                                           //
    //========================================================================================//
    //  Deployment in Reactjs project:                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//
    @PostMapping("/api/v3/reactjs/{token}/{projectName}/{linkGit}/")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> reactjs(@PathVariable("projectName") String projectName,
                                     @PathVariable("token")String token,@RequestParam("linkGit")String linkGit){

        Map<String,Object> response = new HashMap<>();
        System.out.println("linkgit : "+linkGit);
        builder.command("sh", "-c", "bash ./workspace/deploy_reactjs.sh " + projectName+' '+linkGit).inheritIO();
//        builder.command("bash", "-c", "wget http://sourceforge.net/projects/sshpass/files/latest/download -O sshpass.tar.gz\ntar -xvf sshpass.tar.gz\ncd sshpass-1.08\n./configure\nmake install\nsshpass -p 123 ssh -o StrictHostKeyChecking=no devops@34.126.133.229 'bash deploy_angular.sh' "+projectName+' '+linkGit).inheritIO();
        return responseEntity(projectName,token,response);
    }
    //========================================================================================//
    //                                      Angular                                           //
    //========================================================================================//
    //  Deployment in Angular project:                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/api/v3/angular/{token}/{projectName}/{linkGit}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> angular(@PathVariable("projectName") String projectName,
                                     @PathVariable("token")String token,@RequestParam("linkGit")String linkGit){

        Map<String,Object> response = new HashMap<>();
//        builder.command("sh", "-c", "/bin/bash ./shellscript/jks.sh " + projectName).inheritIO();
        builder.command("sh", "-c", "bash ./workspace/deploy_angular.sh " + projectName+' '+linkGit).inheritIO();
        return responseEntity(projectName,token,response);
    }
    //========================================================================================//
    //                                Delete Deploy                                           //
    //========================================================================================//
    //  Delete in any deployment project:                                                     //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @DeleteMapping("/api/v3/deleteDeploy/{token}/{deployName}")
    public ResponseEntity<?> deleteDeploy (@PathVariable("deployName") String deployName,
                                           @PathVariable("token")String token){

        Map<String, Object> response = new HashMap<>();

        builder.command("sh","/bin/bash ./shellscript/deleteDeploy.sh "+deployName);
        return responseEntity(deployName,token,response);
    }


    //========================================================================================//
    //                                Stop Deploy                                             //
    //========================================================================================//
    //  Stop in any deployment project:                                                       //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //                                                                                        //
    //****************************************************************************************//

    @PostMapping("/api/v3/stopDeploy/{token}{deployName}")
    public ResponseEntity<?> stopDeploy(@PathVariable("token")String token,
                                        @PathVariable("deployName") String deployName){

        Map<String, Object>response = new HashMap<>();

        builder.command("sh","/bin/bash ./shellscript/stopDeploy.sh "+deployName);
        return responseEntity(deployName,token,response);
    }
}
