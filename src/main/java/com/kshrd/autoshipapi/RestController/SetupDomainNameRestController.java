package com.kshrd.autoshipapi.RestController;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@RestController
public class SetupDomainNameRestController {

    private final String token1 = "1";
    private final ProcessBuilder builder = new ProcessBuilder();


    //========================================================================================//
    //                                Setup Domain name                                       //
    //========================================================================================//
    //  User អាចធ្វើការ config នៅពេលណាដែល អ្នកមាន domain name ស្រាប់                            //
    //      requirement:                                                                      //
    //          domain name                                                                   //
    //          port                                                                          //
    //          ip address                                                                    //
    //****************************************************************************************//

    @GetMapping("/api/v3/setupDomain/{token}/{domainName}/{ipAddress}/{port}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> setUpDomain(@PathVariable("domainName") String domainName,
                                         @PathVariable("ipAddress") String ipAddress,
                                         @PathVariable("port") String port,
                                         @PathVariable("token")String token){

        Map<String, Object> response = new HashMap<>();
        builder.command("sh", "-c", "bash ./workspace/config_domain.sh " +domainName+' '+ipAddress+' '+port).inheritIO();
        if(Objects.equals(token,token1)){
            if(domainName != null){
                if(port != ""){
                    if(ipAddress != null){
                        try{
                            builder.start();
                        }catch (IOException e){
                            response.put("status", "ERROR");
                            response.put("message", "error exception");
                            response.put("domain name", domainName);
                            response.put("exception",e);
                            response.put("success",false);
                        }
                        response.put("status","success");
                        response.put("message","congratulation! you have successfully set up domain name");
                        response.put("domain name", domainName);
                        response.put("success",true);
                    }else{
                        response.put("status","ERROR");
                        response.put("message","your IP address is not correct");
                        response.put("success",false);
                    }
                }else{
                    response.put("status","ERROR");
                    response.put("message","you need to fill port number");
                    response.put("success",false);
                }
            }else{
                response.put("status","ERROR");
                response.put("message","you need to fill domain name");
                response.put("success",false);
            }
        }else{
            response.put("status", "ERROR");
            response.put("message","permission denied");
            response.put("success",false);
        }
        return ResponseEntity.ok().body(response);
    }


    //========================================================================================//
    //                                  Setup Routing Domain name                             //
    //========================================================================================//
    //  បើសិនជា user មិនមាន domain name។ នោះនិងធ្វើការជា routing domain                          //
    //         requirement:                                                                   //
    //             ipaddress                                                                  //
    //             port                                                                       //
    //                                                                                        //
    //****************************************************************************************//

    @GetMapping("/api/v3/let's_crypte/{token}/{domainName}/{email}")
    @CrossOrigin(origins = "*")
    public ResponseEntity<?> setUpHttps(@PathVariable("domainName") String domdainName,
                                         @PathVariable("email") String email,
                                         @PathVariable("token")String token){

        Map<String, Object> response = new HashMap<>();
        email="povbunnarath19@gmail.com";
        builder.command("sh", "-c", "bash ./workspace/config_https.sh " +domdainName+' '+email).inheritIO();
        if(Objects.equals(token,token1)){
            if(domdainName != null){
                if(email != ""){
                    if(domdainName != null){
                        try{
                            builder.start();
                        }catch (IOException e){
                            response.put("status", "ERROR");
                            response.put("message", "error exception");
                            response.put("domain name", domdainName);
                            response.put("exception",e);
                            response.put("success",false);
                        }
                        response.put("status","success");
                        response.put("message","congratulation! you have successfully set up domain name");
                        response.put("domain name", domdainName);
                        response.put("success",true);
                    }else{
                        response.put("status","ERROR");
                        response.put("message","your IP address is not correct");
                        response.put("success",false);
                    }
                }else{
                    response.put("status","ERROR");
                    response.put("message","you need to fill port number");
                    response.put("success",false);
                }
            }else{
                response.put("status","ERROR");
                response.put("message","you need to fill domain name");
                response.put("success",false);
            }
        }else{
            response.put("status", "ERROR");
            response.put("message","permission denied");
            response.put("success",false);
        }
        return ResponseEntity.ok().body(response);
    }
}
