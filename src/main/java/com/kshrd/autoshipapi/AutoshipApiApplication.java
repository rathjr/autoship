package com.kshrd.autoshipapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AutoshipApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(AutoshipApiApplication.class, args);
    }

}
