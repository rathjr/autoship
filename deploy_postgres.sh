#!/bin/bash

containerName="$1"
databaseName="$2"
userName="$3"
password="$4"
connectionNum="$5"
port="$6"

 docker run -d \
  --restart unless-stopped \
  --name "$containerName" \
  -v "$containerName":/var/lib/postgresql/data \
  -p "$port":5432 \
  -e POSTGRES_DB="$databaseName" \
  -e POSTGRES_USER="$userName" \
  -e POSTGRES_PASSWORD="$password" \
  postgres \
  -c max_connections="$connectionNum"