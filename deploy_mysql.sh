#!/bin/bash

containerName="$1"
databaseName="$2"
userName="$3"
password="$4"
connectionNum="$5"
port="$6"

 docker run -d \
  --restart unless-stopped \
  --name "$containerName" \
  -v "$containerName":/var/lib/mysql/ \
  -p "$port":3306 \
  -e MYSQL_DATABASE="$databaseName" \
  -e MYSQL_ROOT_PASSWORD="$password"\
  -e MYSQL_USER="$userName" \
  -e MYSQL_PASSWORD="$password" \
  mysql \
  --max_connections="$connectionNum"